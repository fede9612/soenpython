from hardware import *
from so import *
import log


##
##  MAIN 
##
if __name__ == '__main__':
    log.setupLogger()
    log.logger.info('Starting emulator')

    ## setup our hardware and set memory size to 25 "cells"
    HARDWARE.setup(60)

    ## Switch on computer
    HARDWARE.switchOn()

    ## new create the Operative System Kernel
    # "booteamos" el sistema operativo
    kernel = Kernel()

    ##  create a program
    prg1 = Program("prg1.exe", [ASM.CPU(2), ASM.CPU(1)])
    prg2 = Program("prg2.exe", [ASM.CPU(4)])
    prg3 = Program("prg3.exe", [ASM.CPU(3)])
    prg4 = Program("prg4.exe", [ASM.CPU(6)])
    batch = [prg1, prg2, prg3, prg4]

    # execute the programs
    kernel.run(prg1,2)
    kernel.run(prg2,3)
    kernel.run(prg3,1)
    kernel.run(prg4,1)




