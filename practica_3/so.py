#!/usr/bin/env python

from hardware import *
import log

TERMINATED = "Terminated"
RUNNING = "Running"
WAITING = "Waiting"
READY = "Ready"

## emulates a compiled program
class Program():

    def __init__(self, name, instructions):
        self._name = name
        self._instructions = self.expand(instructions)

    @property
    def name(self):
        return self._name

    @property
    def instructions(self):
        return self._instructions

    def addInstr(self, instruction):
        self._instructions.append(instruction)

    def expand(self, instructions):
        expanded = []
        for i in instructions:
            if isinstance(i, list):
                ## is a list of instructions
                expanded.extend(i)
            else:
                ## a single instr (a String)
                expanded.append(i)

        ## now test if last instruction is EXIT
        ## if not... add an EXIT as final instruction
        last = expanded[-1]
        if not ASM.isEXIT(last):
            expanded.append(INSTRUCTION_EXIT)

        return expanded

    def __repr__(self):
        return "Program({name}, {instructions})".format(name=self._name, instructions=self._instructions)


## emulates an Input/Output device controller (driver)
class IoDeviceController():

    def __init__(self, device):
        self._device = device
        self._waiting_queue = []
        self._currentPCB = None

    def runOperation(self, pcb, instruction):
        pair = {'pcb': pcb, 'instruction': instruction}
        # append: adds the element at the end of the queue
        self._waiting_queue.append(pair)
        # try to send the instruction to hardware's device (if is idle)
        self.__load_from_waiting_queue_if_apply()

    def getFinishedPCB(self):
        finishedPCB = self._currentPCB
        self._currentPCB = None
        self.__load_from_waiting_queue_if_apply()
        return finishedPCB

    def __load_from_waiting_queue_if_apply(self):
        if (len(self._waiting_queue) > 0) and self._device.is_idle:
            ## pop(): extracts (deletes and return) the first element in queue
            pair = self._waiting_queue.pop(0)
            #print(pair)
            pcb = pair['pcb']
            instruction = pair['instruction']
            self._currentPCB = pcb
            self._device.execute(instruction)


    def __repr__(self):
        return "IoDeviceController for {deviceID} running: {currentPCB} waiting: {waiting_queue}".format(deviceID=self._device.deviceId, currentPCB=self._currentPCB, waiting_queue=self._waiting_queue)

## emulates the  Interruptions Handlers
class AbstractInterruptionHandler():
    def __init__(self, kernel):
        self._kernel = kernel

    @property
    def kernel(self):
        return self._kernel

    def execute(self, irq):
        log.logger.error("-- EXECUTE MUST BE OVERRIDEN in class {classname}".format(classname=self.__class__.__name__))


class KillInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        self.kernel.pcbtable.liberarCPUSiTermino()


        if(self.kernel.pcbtable.todosTerminaron()):
            log.logger.info(" Program Finished ")
            # por ahora apagamos el hardware porque estamos ejecutando un solo programa
            HARDWARE.switchOff()
            log.logger.info(self.kernel.gantt)
        else :
            if(self.kernel.pcbtable.hayAlgunoEnReady()):
                next_pcb = self.kernel.quitarPrimeroDelReadyQueue()
                next_pcb.running()
                self.kernel.dispatcher.load(next_pcb)


class IoInInterruptionHandler(AbstractInterruptionHandler):

    def execute(self, irq):
        #acá tengo que armar un diccionario, con el pcb y al irq.parameters
        operation = irq.parameters
        pcb = self.kernel.pcbtable.pcbQueEstaRunning()
        self.kernel.dispatcher.save(pcb)
        log.logger.info("Nombre: {}".format(pcb.programPath))
        pcb.waiting()
        if(not HARDWARE.ioDevice.is_busy):
            HARDWARE.ioDevice.execute(operation)
        self.kernel.ioController.add({"pcb" : pcb, "operation" : operation})
        if(self.kernel.pcbtable.hayAlgunoEnReady()):
            next_pcb = self.kernel.quitarPrimeroDelReadyQueue()
            next_pcb.running()
            self.kernel.dispatcher.load(next_pcb)


class IoOutInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        diccionarioConProgramaSalidoDeIO = self.kernel.ioController.pop()
        pcb = diccionarioConProgramaSalidoDeIO["pcb"]
        operation = diccionarioConProgramaSalidoDeIO["operation"]
        if(HARDWARE.cpu.pc == -1):
            pcb.running()
            self.kernel.dispatcher.load(pcb)
        else :
            pcb.ready()
            self.kernel.scheduler.readyQueue.append(pcb)
        if(self.kernel.ioController.waitingQueue()):
            diccionarioConSiguiente = self.kernel.ioController.top()
            pcb_next = diccionarioConSiguiente["pcb"]
            operation_net = diccionarioConSiguiente
            HARDWARE.ioDevice.execute(operation_net)


# emulates the core of an Operative System
class Kernel():

    def __init__(self):
        self.loader = Loader()
        self.dispatcher = Dispatcher()
        self.pcbtable = PCBTable(self)
        self.pid = -1
        self.gantt = Gantt(self)
        self.ioController = IOController(self)
        # self.scheduler = Fcfs(self)
        # self.scheduler = PrioridadNoExpropiativa(self)
        # self.scheduler = PrioridadExpropiativa(self)
        self.scheduler = RoundRobin(self)
        
        HARDWARE._clock.addSubscriber(self.gantt)

        ## setup interruption handlers
        killHandler = KillInterruptionHandler(self)
        HARDWARE.interruptVector.register(KILL_INTERRUPTION_TYPE, killHandler)

        ioInHandler = IoInInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_IN_INTERRUPTION_TYPE, ioInHandler)

        ioOutHandler = IoOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(IO_OUT_INTERRUPTION_TYPE, ioOutHandler)

        newHandler= NewInterruptionHandler(self)
        HARDWARE.interruptVector.register(NEW_INTERRUPTION_TYPE, newHandler)

        timerHandler= TimerOutInterruptionHandler(self)
        HARDWARE.interruptVector.register(TIMEOUT_INTERRUPTION_TYPE, timerHandler)
        HARDWARE.timer.quantum = 3

        ## controls the Hardware's I/O Device
        self._ioDeviceController = IoDeviceController(HARDWARE.ioDevice)


    @property
    def ioDeviceController(self):
        return self._ioDeviceController

    def load_program(self, program):
        # loads the program in main memory
        progSize = len(program.instructions)
        for index in range(0, progSize):
            inst = program.instructions[index]
            HARDWARE.memory.write(index, inst)

    ## emulates a "system call" for programs execution
    def run(self, program, prioridad=1):
        log.logger.info("la prioridad en el run {}". format(prioridad))
        newIRQ= IRQ(NEW_INTERRUPTION_TYPE, {"program": program, "priority": prioridad})
        HARDWARE.interruptVector.handle(newIRQ)

        
        # set CPU program counter at program's first intruction
        

    def run_batch(self, batch):
        for program in batch: 
            self.run(program)

    def quitarPrimeroDelReadyQueue(self):
        return self.scheduler.readyQueue.pop(0)

    def __repr__(self):
        # return "Kernel "
        return tabulate(enumerate(self.pcbtable._pcbs), tablefmt='psql')

class Loader :
    
    def __init__(self):
        self.lastMemory = 0;

    def load(self, program):
        programSize = len(program.instructions)
        for index in range(0, programSize):
            inst = program.instructions[index]
            HARDWARE.memory.write((index + self.lastMemory), inst)
        self.lastMemory += programSize
        return self.lastMemory - programSize

class PCB :

    def __init__(self, baseDir, programPath, pid, priority):
        self._baseDir = baseDir
        self._programPath = programPath
        self._pc = 0
        self._pid = pid
        self._estado = ""
        self._priority = priority

    @property
    def pid(self):
        return self._pid

    @property
    def estado(self):
        return self._estado
    
    @property
    def programPath(self):
        return self._programPath

    @property
    def priority(self):
        return self._priority

    def running(self):
        self._estado = RUNNING

    def ready(self):
        self._estado = READY

    def waiting(self):
        self._estado = WAITING

    def terminated(self):
        self._estado = TERMINATED

    def __repr__(self):
        return self._programPath + " " + str(self._baseDir) + " " + self._estado
    
class PCBTable :

    def __init__(self, kernel):
        self._pcbs = []
        self._kernel = kernel

    def add(self, pcb):
        self._pcbs.append(pcb)

    @property
    def pcbs(self):
        return self._pcbs

    def deleteFirts(self):
        return self._pcbs.pop(0)

    def todosTerminaron(self):
        terminaron = False
        for pcb in self._pcbs:
            if(pcb.estado == TERMINATED):
                terminaron = True
            else :
                return False
        return terminaron

    def liberarCPUSiTermino(self):
        for pcb in self._pcbs:
            if(pcb.estado == RUNNING):
                pcb.terminated()
                self._kernel.dispatcher.save(pcb)
    
    def pcbQueEstaRunning(self):
        for pcb in self._pcbs:
            if(pcb.estado == RUNNING):
                return pcb
    
    def hayAlgunoEnReady(self):
        for pcb in self._pcbs:
            if(pcb.estado == READY):
                return True                


class Dispatcher :

    def __init__(self):
        self._pcb = None 
        
    def load(self, pcb):
        HARDWARE.cpu.pc = 0
        HARDWARE.cpu.pc = pcb._pc
        HARDWARE.mmu.baseDir = pcb._baseDir
        pcb.running()
        self._pcb = pcb

    def save(self, pcb):
        pcb._pc = HARDWARE.cpu.pc
        HARDWARE.cpu.pc = -1
        self._pcb = None
    
    @property
    def pcb(self):
        return self._pcb


class Gantt :
    
    def __init__(self,kernel):
        self._ticks = []
        self._kernel = kernel
   
    def tick (self,tickNbr):
        log.logger.info("guardando informacion de los estados de los PCBs en el tick N {}".format(tickNbr))
        pcbYEstado = dict()
        pcbTable = self._kernel.pcbtable.pcbs
        for pcb in pcbTable:
            log.logger.info("PID: {} Nombre: {} Estado: {}".format(pcb.pid, pcb.programPath, pcb.estado))
            pcbYEstado[pcb.pid] = pcb.estado
        self._ticks.append(pcbYEstado)
  
    def __repr__(self):
        return tabulate(enumerate(self._ticks), tablefmt='grid')

class IOController :

    def __init__(self, kernel):
        self._waitingQueue = []
        self._kernel = kernel

    def add(self, pcbYOperation):
        self._waitingQueue.append(pcbYOperation)
    
    def pop(self):
        return self._waitingQueue.pop(0)
    
    def top(self):
        return self._waitingQueue[0]

    def waitingQueue(self):
        return self._waitingQueue

    def runPCBSiHayEnWaiting(self):
         if(self._waitingQueue):
                if(HARDWARE.cpu.pc == -1):
                    pcb_next = self.pop()
                    pcb_next.running()
                    self._kernel.dispatcher.load(pcb_next)

class NewInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        _program = irq.parameters["program"]
        _priority= irq.parameters["priority"]
        baseDir = self.kernel.loader.load(_program)
        pcb1 = PCB(baseDir, _program.name, (self.kernel.pid+1), _priority)
        self.kernel.pid += 1
        self.kernel.pcbtable.add(pcb1)
        log.logger.info("\n Executing program: {name}".format(name=_program.name))
        log.logger.info(HARDWARE)
        log.logger.info("\n PCB Table {}".format(self))
        self.kernel.scheduler.addPcb(pcb1)
        log.logger.info(HARDWARE)

class AbstractScheduler():
    def __init__(self,_kernel):
        self._readyQueue = []
        self._kernel = _kernel

    @property
    def readyQueue(self):
        return self._readyQueue

    def addPcb(self,pcb):
        pass
        
    def nextPcb(self):    
        return self.readyQueue.dequeue()
        
    def mustExpropiet(self,pcb):    
        pass


class Fcfs(AbstractScheduler):

    def __init__(self, _kernel):
        AbstractScheduler.__init__(self, _kernel)

    def addPcb(self,pcb):
        log.logger.info("la prioridad del pcb es : {}".format(pcb.priority))
        log.logger.info("la prioridad despues de cambiarla al  pcb es : {}".format(pcb.priority))
        if(HARDWARE.cpu.pc == -1):
            self._kernel.dispatcher.load(pcb)
        else :
            self.readyQueue.append(pcb)
            pcb.ready()
       
    def mustExpropiet(self,pcb):    
        pass

class PrioridadNoExpropiativa(AbstractScheduler):

    def __init__(self, _kernel):
        AbstractScheduler.__init__(self, _kernel)

    def addPcb(self,pcb):
        log.logger.info("la prioridad del pcb es : {}".format(pcb.priority))
        log.logger.info("la prioridad despues de cambiarla al  pcb es : {}".format(pcb.priority))
        if(HARDWARE.cpu.pc == -1):
            self._kernel.dispatcher.load(pcb)
        else :
            i = 0
            lenghtReadyQueue = len(self.readyQueue)
            while(i < lenghtReadyQueue and pcb.priority >= self.readyQueue[i].priority):
                i += 1
            self.readyQueue.insert(i, pcb)
            pcb.ready()
       
    def mustExpropiet(self,pcb):    
        pass

class PrioridadExpropiativa(AbstractScheduler):
    
    def __init__(self, _kernel):
        AbstractScheduler.__init__(self, _kernel)

    def addPcb(self,pcb):
        log.logger.info("la prioridad del pcb es : {}".format(pcb.priority))
        log.logger.info("la prioridad despues de cambiarla al  pcb es : {}".format(pcb.priority))
        if(HARDWARE.cpu.pc == -1):
            self._kernel.dispatcher.load(pcb)
        else :
            i = 0
            lenghtReadyQueue = len(self.readyQueue)
            if(pcb.priority < self._kernel.dispatcher.pcb.priority):
                pcbTemporary = self._kernel.dispatcher.pcb
                self._kernel.dispatcher.save(pcbTemporary)
                self._kernel.dispatcher.load(pcb)
                pcb = pcbTemporary
            while(i < lenghtReadyQueue and pcb.priority >= self.readyQueue[i].priority):
                i += 1
            self.readyQueue.insert(i, pcb)
            pcb.ready()
       
    def mustExpropiet(self,pcb):    
        pass

class RoundRobin(AbstractScheduler):
    
    def __init__(self, _kernel):
        AbstractScheduler.__init__(self, _kernel)

    def addPcb(self,pcb):
        log.logger.info("la prioridad del pcb es : {}".format(pcb.priority))
        log.logger.info("la prioridad despues de cambiarla al  pcb es : {}".format(pcb.priority))
        if(HARDWARE.cpu.pc == -1):
            self._kernel.dispatcher.load(pcb)
        else :
            self.readyQueue.append(pcb)
            pcb.ready()
       
    def mustExpropiet(self,pcb):    
        pass

class TimerOutInterruptionHandler(AbstractInterruptionHandler):
    def execute(self, irq):
        if(self.kernel.scheduler.readyQueue):
            pcbTemporary = self.kernel.dispatcher.pcb
            if(pcbTemporary):
                pcbTemporary.ready()
                self.kernel.dispatcher.save(pcbTemporary)
                self.kernel.scheduler.readyQueue.append(pcbTemporary)
            pcbNext = self.kernel.quitarPrimeroDelReadyQueue()
            self.kernel.dispatcher.load(pcbNext)
        HARDWARE.timer.reset()
